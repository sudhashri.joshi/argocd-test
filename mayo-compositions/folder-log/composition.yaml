apiVersion: apiextensions.crossplane.io/v1
kind: Composition
metadata:
  name: xfolderlogs.gcp.mcp.org
  labels:
    cloud/provider: gcp
spec:
  writeConnectionSecretsToNamespace: crossplane-system
  compositeTypeRef:
    apiVersion: gcp.mcp.org/v1alpha1
    kind: XFolderLog
  patchSets:
    - name: metadata
      patches:
        - type: FromCompositeFieldPath
          fromFieldPath: metadata.labels
          toFieldPath: metadata.labels
          policy:
            mergeOptions:
              keepMapValues: true
        - type: FromCompositeFieldPath
          fromFieldPath: metadata.annotations
          toFieldPath: metadata.annotations
          policy:
            mergeOptions:
              keepMapValues: true
        - type: CombineFromComposite
          combine:
            variables:
              - fromFieldPath: spec.claimRef.namespace
              - fromFieldPath: spec.claimRef.name
            strategy: string
            string:
              fmt: "%s-%s"
          toFieldPath: metadata.labels["folderlog.gcp.mcp.org/folderlog-ref"]
    - name: providerConfigRef
      patches:
        - type: FromCompositeFieldPath
          fromFieldPath: spec.claimRef.namespace
          toFieldPath: spec.providerConfigRef.name

  resources:
    - name: pubsub_topic
      base:
        apiVersion: pubsub.gcp.upbound.io/v1beta1
        kind: Topic
        spec:
          deletionPolicy: Delete
      patches:
        - type: FromCompositeFieldPath
          fromFieldPath: spec.parameters.name
          toFieldPath: metadata.annotations["crossplane.io/external-name"]
        - type: PatchSet
          patchSetName: metadata
        - type: PatchSet
          patchSetName: providerConfigRef
        - type: FromCompositeFieldPath
          toFieldPath: spec.forProvider.project
          fromFieldPath: spec.parameters.topic.project
        - type: ToCompositeFieldPath
          fromFieldPath: status.atProvider
          toFieldPath: status.topic.atProvider
        - type: ToCompositeFieldPath
          fromFieldPath: status.conditions
          toFieldPath: status.topic.conditions

    - name: folder_sink
      base:
        apiVersion: logging.gcp.upbound.io/v1beta1
        kind: FolderSink
        spec:
          deletionPolicy: Delete
      patches:
        - type: PatchSet
          patchSetName: metadata
        - type: PatchSet
          patchSetName: providerConfigRef
        - type: FromCompositeFieldPath
          fromFieldPath: spec.parameters.name
          toFieldPath: metadata.annotations["crossplane.io/external-name"]
        - type: CombineFromComposite
          combine:
            variables:
              - fromFieldPath: spec.parameters.topic.project
              - fromFieldPath: spec.parameters.name
            strategy: string
            string:
              fmt: "pubsub.googleapis.com/projects/%s/topics/%s"
          toFieldPath: spec.forProvider.destination
        - type: FromCompositeFieldPath
          fromFieldPath: spec.parameters.folderSink.folder
          toFieldPath: spec.forProvider.folder
        - type: FromCompositeFieldPath
          fromFieldPath: spec.parameters.folderSink.filter
          toFieldPath: spec.forProvider.filter
        - type: FromCompositeFieldPath
          fromFieldPath: spec.parameters.folderSink.includeChildren
          toFieldPath: spec.forProvider.includeChildren
        - type: ToCompositeFieldPath
          fromFieldPath: status.atProvider
          toFieldPath: status.foldersink.atProvider
        - type: ToCompositeFieldPath
          fromFieldPath: status.conditions
          toFieldPath: status.foldersink.conditions

    - name: topic_iam_member
      base:
        apiVersion: pubsub.gcp.upbound.io/v1beta1
        kind: TopicIAMMember
        spec:
          deletionPolicy: Delete
          forProvider:
            topicSelector:
              matchControllerRef: true
            role: roles/pubsub.publisher
            # role: roles/logging.logWriter
      patches:
        - type: FromCompositeFieldPath
          fromFieldPath: status.foldersink.atProvider.writerIdentity
          toFieldPath: spec.forProvider.member
          # transforms:
          #   - type: string
          #     string:
          #       fmt: "serviceAccount:%s"
          policy:
            fromFieldPath: Required
        - type: PatchSet
          patchSetName: metadata
        - type: PatchSet
          patchSetName: providerConfigRef
        - type: ToCompositeFieldPath
          fromFieldPath: status.atProvider
          toFieldPath: status.topiciammember.atProvider
        - type: ToCompositeFieldPath
          fromFieldPath: status.conditions
          toFieldPath: status.topiciammember.conditions

    - name: pull_subscription
      base:
        apiVersion: pubsub.gcp.upbound.io/v1beta1
        kind: Subscription
        spec:
          deletionPolicy: Delete
          forProvider:
            topicSelector:
              matchControllerRef: true
      patches:
      - type: FromCompositeFieldPath
        fromFieldPath: spec.parameters.subscription.ackDeadlineSeconds
        toFieldPath: spec.forProvider.ackDeadlineSeconds
      - type: FromCompositeFieldPath
        fromFieldPath: spec.parameters.subscription.messageRetentionDuration
        toFieldPath: spec.forProvider.messageRetentionDuration
      - type: PatchSet
        patchSetName: metadata
      - type: PatchSet
        patchSetName: providerConfigRef
      - type: ToCompositeFieldPath
        fromFieldPath: status.atProvider
        toFieldPath: status.subscription.atProvider
      - type: ToCompositeFieldPath
        fromFieldPath: status.conditions
        toFieldPath: status.subscription.conditions
