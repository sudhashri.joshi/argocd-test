#!/bin/bash

export PROJECTID=sudhatest-vpc-ae042c35
export CREDS=$HOME/.config/gcloud/application_default_credentials.json


kubectl -n default create secret generic gcp-auth --from-file=creds=$CREDS