apiVersion: apiextensions.crossplane.io/v1
kind: Composition
metadata:
  name: xnetworkbases.gcp.network.sandbox.mcp.org
  labels:
    crossplane.io/xrd: xnetworkbases.gcp.network.sandbox.mcp.org
    provider: gcp
spec:
  compositeTypeRef:
    apiVersion: gcp.network.sandbox.mcp.org/v1alpha1
    kind: XNetworkBase
  writeConnectionSecretsToNamespace: crossplane-system
  patchSets:
  - name: metadata
    patches:
    - type: FromCompositeFieldPath
      fromFieldPath: metadata.annotations
      toFieldPath: metadata.annotations
      policy:
        mergeOptions:
          keepMapValues: true
    - type: FromCompositeFieldPath
      fromFieldPath: metadata.labels
      toFieldPath: metadata.labels
      policy:
        mergeOptions:
          keepMapValues: true
    - type: CombineFromComposite
      combine:
        variables:
        - fromFieldPath: spec.claimRef.namespace
        - fromFieldPath: spec.claimRef.name
        strategy: string
        string:
          fmt: '%s-%s'
      toFieldPath: metadata.labels["gcp.network.sandbox.mcp.org/network-base-ref"]
      policy:
        mergeOptions:
          keepMapValues: true
  - name: providerConfigRef
    patches:
    - type: CombineFromComposite
      combine:
        variables:
        - fromFieldPath: spec.claimRef.namespace
        - fromFieldPath: spec.parameters.projectId
        strategy: string
        string:
          fmt: '%s-%s'
      toFieldPath: spec.providerConfigRef.name
  resources:
  - name: network
    base:
      apiVersion: compute.gcp.upbound.io/v1beta1
      kind: Network
      spec:
        forProvider:
          autoCreateSubnetworks: false
          routingMode: GLOBAL
    patches:
    - type: PatchSet
      patchSetName: metadata
    - type: PatchSet
      patchSetName: providerConfigRef
    - type: FromCompositeFieldPath
      fromFieldPath: spec.parameters.deletionPolicy
      toFieldPath: spec.deletionPolicy
    - type: CombineFromComposite
      combine:
        variables:
        - fromFieldPath: spec.parameters.tenant
        - fromFieldPath: spec.parameters.purpose
        strategy: string
        string:
          fmt: 'mcp-%s-%s-n'
      toFieldPath: metadata.annotations["crossplane.io/external-name"]
      policy:
        mergeOptions:
          keepMapValues: true
    - type: FromCompositeFieldPath
      fromFieldPath: spec.parameters.tenant
      toFieldPath: spec.forProvider.description
      transforms:
        - type: string
          string:
            fmt: 'Default network for %s'
    - type: ToCompositeFieldPath
      fromFieldPath: status.atProvider
      toFieldPath: status.network.atProvider
    - type: ToCompositeFieldPath
      fromFieldPath: status.conditions
      toFieldPath: status.network.conditions

  - name: subnetwork
    base:
      apiVersion: compute.gcp.upbound.io/v1beta1
      kind: Subnetwork
      spec:
        forProvider:
          ipCidrRange: 10.10.0.0/24
          logConfig:
          - metadata: INCLUDE_ALL_METADATA
          networkSelector:
            matchControllerRef: true
          privateIpGoogleAccess: true
          region: us-central1
    patches:
    - type: PatchSet
      patchSetName: metadata
    - type: PatchSet
      patchSetName: providerConfigRef
    - type: FromCompositeFieldPath
      fromFieldPath: spec.parameters.deletionPolicy
      toFieldPath: spec.deletionPolicy
    - type: CombineFromComposite
      combine:
        variables:
        - fromFieldPath: spec.parameters.tenant
        - fromFieldPath: spec.parameters.purpose
        strategy: string
        string:
          fmt: 'mcp-%s-%s-n-default'
      toFieldPath: metadata.annotations["crossplane.io/external-name"]
      policy:
        mergeOptions:
          keepMapValues: true
    - type: FromCompositeFieldPath
      fromFieldPath: spec.parameters.tenant
      toFieldPath: spec.forProvider.description
      transforms:
        - type: string
          string:
            fmt: 'Default subnetwork for %s'
    - type: ToCompositeFieldPath
      fromFieldPath: status.atProvider
      toFieldPath: status.subnetwork.atProvider
    - type: ToCompositeFieldPath
      fromFieldPath: status.conditions
      toFieldPath: status.subnetwork.conditions

  - name: router
    base:
      apiVersion: compute.gcp.upbound.io/v1beta1
      kind: Router
      spec:
        forProvider:
          networkSelector:
            matchControllerRef: true
          region: us-central1
    patches:
    - type: PatchSet
      patchSetName: metadata
    - type: PatchSet
      patchSetName: providerConfigRef
    - type: FromCompositeFieldPath
      fromFieldPath: spec.parameters.deletionPolicy
      toFieldPath: spec.deletionPolicy
    - type: CombineFromComposite
      combine:
        variables:
        - fromFieldPath: spec.parameters.tenant
        - fromFieldPath: spec.parameters.purpose
        strategy: string
        string:
          fmt: 'mcp-%s-%s-n-nat-router'
      toFieldPath: metadata.annotations["crossplane.io/external-name"]
      policy:
        mergeOptions:
          keepMapValues: true
    - type: FromCompositeFieldPath
      fromFieldPath: spec.parameters.tenant
      toFieldPath: spec.forProvider.description
      transforms:
        - type: string
          string:
            fmt: 'Default router for %s'
    - type: ToCompositeFieldPath
      fromFieldPath: status.atProvider
      toFieldPath: status.router.atProvider
    - type: ToCompositeFieldPath
      fromFieldPath: status.conditions
      toFieldPath: status.router.conditions

  - name: routernat
    base:
      apiVersion: compute.gcp.upbound.io/v1beta1
      kind: RouterNAT
      spec:
        forProvider:
          logConfig:
          - enable: true
            filter: ALL
          natIpAllocateOption: AUTO_ONLY
          region: us-central1
          routerSelector:
            matchControllerRef: true
          sourceSubnetworkIpRangesToNat: ALL_SUBNETWORKS_ALL_IP_RANGES
    patches:
    - type: PatchSet
      patchSetName: metadata
    - type: PatchSet
      patchSetName: providerConfigRef
    - type: FromCompositeFieldPath
      fromFieldPath: spec.parameters.deletionPolicy
      toFieldPath: spec.deletionPolicy
    - type: CombineFromComposite
      combine:
        variables:
        - fromFieldPath: spec.parameters.tenant
        - fromFieldPath: spec.parameters.purpose
        strategy: string
        string:
          fmt: 'mcp-%s-%s-n-nat-router'
      toFieldPath: metadata.annotations["crossplane.io/external-name"]
      policy:
        mergeOptions:
          keepMapValues: true
    - type: ToCompositeFieldPath
      fromFieldPath: status.atProvider
      toFieldPath: status.nat.atProvider
    - type: ToCompositeFieldPath
      fromFieldPath: status.conditions
      toFieldPath: status.nat.conditions

  - name: firewall-egress-deny-all
    base:
      apiVersion: compute.gcp.upbound.io/v1beta1
      kind: Firewall
      spec:
        forProvider:
          deny:
            - protocol: all
          description: egress deny all
          destinationRanges:
          - "0.0.0.0/0"
          direction: EGRESS
          logConfig:
          - metadata: INCLUDE_ALL_METADATA
          networkSelector:
            matchControllerRef: true
          priority: 65534
    patches:
    - type: PatchSet
      patchSetName: metadata
    - type: PatchSet
      patchSetName: providerConfigRef
    - type: FromCompositeFieldPath
      fromFieldPath: spec.parameters.deletionPolicy
      toFieldPath: spec.deletionPolicy
    - type: FromCompositeFieldPath
      fromFieldPath: metadata.name
      toFieldPath: metadata.name
      transforms:
        - type: string
          string:
            fmt: '%s-firewall-egress-deny-all'
    - type: CombineFromComposite
      combine:
        variables:
        - fromFieldPath: spec.parameters.tenant
        - fromFieldPath: spec.parameters.purpose
        strategy: string
        string:
          fmt: 'egress-deny-mcp-%s-%s-n-to-internet'
      toFieldPath: metadata.annotations["crossplane.io/external-name"]
      policy:
        mergeOptions:
          keepMapValues: true
    - type: ToCompositeFieldPath
      fromFieldPath: status.atProvider
      toFieldPath: status.firewalls[0].atProvider
    - type: ToCompositeFieldPath
      fromFieldPath: status.conditions
      toFieldPath: status.firewalls[0].conditions

  - name: firewall-egress-allow-https
    base:
      apiVersion: compute.gcp.upbound.io/v1beta1
      kind: Firewall
      spec:
        forProvider:
          allow:
            - protocol: tcp
              ports: 
              - "443"
          description: egress allow https
          destinationRanges:
          - "0.0.0.0/0"
          direction: EGRESS
          logConfig:
          - metadata: INCLUDE_ALL_METADATA
          networkSelector:
            matchControllerRef: true
          priority: 1000
    patches:
    - type: PatchSet
      patchSetName: metadata
    - type: PatchSet
      patchSetName: providerConfigRef
    - type: FromCompositeFieldPath
      fromFieldPath: spec.parameters.deletionPolicy
      toFieldPath: spec.deletionPolicy
    - type: FromCompositeFieldPath
      fromFieldPath: metadata.name
      toFieldPath: metadata.name
      transforms:
        - type: string
          string:
            fmt: '%s-egress-allow-https-internet-access'
    - type: CombineFromComposite
      combine:
        variables:
        - fromFieldPath: spec.parameters.tenant
        - fromFieldPath: spec.parameters.purpose
        strategy: string
        string:
          fmt: 'egress-allow-https-mcp-%s-%s-n-internet-access'
      toFieldPath: metadata.annotations["crossplane.io/external-name"]
      policy:
        mergeOptions:
          keepMapValues: true
    - type: ToCompositeFieldPath
      fromFieldPath: status.atProvider
      toFieldPath: status.firewalls[1].atProvider
    - type: ToCompositeFieldPath
      fromFieldPath: status.conditions
      toFieldPath: status.firewalls[1].conditions

  - name: firewall-ingress-allow-iap
    base:
      apiVersion: compute.gcp.upbound.io/v1beta1
      kind: Firewall
      spec:
        forProvider:
          allow:
            - protocol: tcp
              ports: 
              - "22"
          description: ingress allow iap
          sourceRanges:
          - "35.235.240.0/20"
          direction: INGRESS
          logConfig:
          - metadata: INCLUDE_ALL_METADATA
          networkSelector:
            matchControllerRef: true
          priority: 1000
    patches:
    - type: PatchSet
      patchSetName: metadata
    - type: PatchSet
      patchSetName: providerConfigRef
    - type: FromCompositeFieldPath
      fromFieldPath: spec.parameters.deletionPolicy
      toFieldPath: spec.deletionPolicy
    - type: FromCompositeFieldPath
      fromFieldPath: metadata.name
      toFieldPath: metadata.name
      transforms:
        - type: string
          string:
            fmt: '%s-ingress-allow-iap'
    - type: CombineFromComposite
      combine:
        variables:
        - fromFieldPath: spec.parameters.tenant
        - fromFieldPath: spec.parameters.purpose
        strategy: string
        string:
          fmt: 'ingress-allow-enable-mcp-%s-%s-n-iap'
      toFieldPath: metadata.annotations["crossplane.io/external-name"]
      policy:
        mergeOptions:
          keepMapValues: true
    - type: ToCompositeFieldPath
      fromFieldPath: status.atProvider
      toFieldPath: status.firewalls[2].atProvider
    - type: ToCompositeFieldPath
      fromFieldPath: status.conditions
      toFieldPath: status.firewalls[2].conditions