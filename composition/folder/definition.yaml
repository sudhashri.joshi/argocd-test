apiVersion: apiextensions.crossplane.io/v1
kind: CompositeResourceDefinition
metadata:
  name: xfolders.gcp.mcp.mayo.org
spec:
  group: gcp.mcp.mayo.org
  names:
    kind: XFolder
    plural: xfolders
  claimNames:
    kind: FolderClaim
    plural: folderclaims
  defaultCompositionRef:
    name: xfolders.gcp.mcp.mayo.org
  versions:
  - name: v1alpha1
    served: true
    referenceable: true
    schema:
      openAPIV3Schema:
        type: object
        properties:
          spec:
            type: object
            properties:
              parameters:
                type: object
                required:
                - displayName
                properties:
                  deletionPolicy:
                    type: string
                    description: 'DeletionPolicy specifies what will happen to the underlying
                      external when this managed resource is deleted - either "Delete"
                      or "Orphan" the external resource. This field is planned to be deprecated
                      in favor of the ManagementPolicy field in a future release. Currently,
                      both could be set independently and non-default values would be
                      honored if the feature flag is enabled. See the design doc for more
                      information: https://github.com/crossplane/crossplane/blob/499895a25d1a1a0ba1604944ef98ac7a1a71f197/design/design-doc-observe-only-resources.md?plain=1#L223'
                    enum:
                    - Orphan
                    - Delete
                    default: Delete
                  displayName:
                    type: string
                    description: The folder’s display name. A folder’s display name
                      must be unique amongst its siblings, e.g. no two folders with
                      the same parent can share the same display name. The display
                      name must start and end with a letter or digit, may contain
                      letters, digits, spaces, hyphens and underscores and can be
                      no longer than 30 characters.
                  parent:
                    type: string
                    description: The resource name of the parent Folder or Organization.
                      Must be of the form folders/{folder_id} or organizations/{org_id}.
                  parentRef:
                    type: object
                    description: Reference to a Folder in cloudplatform to populate
                      parent.
                    required:
                    - name
                    properties:
                      name:
                        type: string
                        description: Name of the referenced object.
                      policy:
                        type: object
                        description: Policies for referencing.
                        properties:
                          resolution:
                            type: string
                            description: Resolution specifies whether resolution of
                              this reference is required. The default is 'Required',
                              which means the reconcile will fail if the reference
                              cannot be resolved. 'Optional' means this reference
                              will be a no-op if it cannot be resolved.
                            enum:
                            - Required
                            - Optional
                            default: Required
                          resolve:
                            type: string
                            description: Resolve specifies when this reference should
                              be resolved. The default is 'IfNotPresent', which will
                              attempt to resolve the reference only when the corresponding
                              field is not present. Use 'Always' to resolve the reference
                              on every reconcile.
                            enum:
                            - Always
                            - IfNotPresent
                  parentSelector:
                    type: object
                    description: Selector for a Folder in cloudplatform to populate
                      parent.
                    properties:
                      matchLabels:
                        type: object
                        additionalProperties:
                          type: string
                          description: MatchLabels ensures an object with matching labels is selected.
                      policy:
                        type: object
                        description: Policies for selection.
                        properties:
                          resolution:
                            type: string
                            description: Resolution specifies whether resolution of
                              this reference is required. The default is 'Required',
                              which means the reconcile will fail if the reference
                              cannot be resolved. 'Optional' means this reference
                              will be a no-op if it cannot be resolved.
                            enum:
                            - Required
                            - Optional
                            default: Required
                          resolve:
                            type: string
                            description: Resolve specifies when this reference should
                              be resolved. The default is 'IfNotPresent', which will
                              attempt to resolve the reference only when the corresponding
                              field is not present. Use 'Always' to resolve the reference
                              on every reconcile.
                            enum:
                            - Always
                            - IfNotPresent

          status:
            type: object
            description: FolderStatus defines the observed state of Folder.
            properties:
              atProvider:
                type: object
                properties:
                  displayName:
                    type: string
                    description: The folder’s display name. A folder’s display name
                      must be unique amongst its siblings, e.g. no two folders with
                      the same parent can share the same display name. The display
                      name must start and end with a letter or digit, may contain
                      letters, digits, spaces, hyphens and underscores and can be
                      no longer than 30 characters.
                  folderId:
                    type: string
                  id:
                    type: string
                  lifecycleState:
                    type: string
                    description: The lifecycle state of the folder such as ACTIVE
                      or DELETE_REQUESTED.
                  name:
                    type: string
                    description: The resource name of the Folder. Its format is folders/{folder_id}.
                  parent:
                    type: string
                    description: The resource name of the parent Folder or Organization.
                      Must be of the form folders/{folder_id} or organizations/{org_id}.
              conditions:
                type: array
                description: Conditions of the resource.
                items:
                  description: A Condition that may apply to a resource.
                  type: object
                  required:
                  - lastTransitionTime
                  - reason
                  - status
                  - type
                  properties:
                    lastTransitionTime:
                      type: string
                      description: LastTransitionTime is the last time this condition
                        transitioned from one status to another.
                      format: date-time
                    message:
                      type: string
                      description: A Message containing details about this condition's
                        last transition from one status to another, if any.
                    reason:
                      type: string
                      description: A Reason for this condition's last transition from
                        one status to another.
                    status:
                      type: string
                      description: Status of this condition; is it currently True,
                        False, or Unknown?
                    type:
                      type: string
                      description: Type of this condition. At most one of each condition
                        type may apply to a resource at any point in time.

    additionalPrinterColumns:
    - jsonPath: .status.atProvider.folderId
      name: ID
      type: string